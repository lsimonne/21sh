/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 13:57:19 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 03:53:10 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include "command_line.h"

void	*memalloc_or_die(size_t size)
{
	void	*m;

	if (!(m = ft_memalloc(size)))
	{
		ft_putendl("Malloc error");
		exit(-1);
	}
	return (m);
}

int		main(int ac, char **av, char **envp)
{
	t_sh	*sh;
	void	(*sig)(int);
	// char	*line;

	sig = ft_signals;
	(void)ac;
	(void)av;
	sh = (t_sh *)memalloc_or_die(sizeof(t_sh));
	if (ft_init(sh, envp) < 0)
		ft_putendl("init problem");
	ft_putstr(sh->prompt);
		while (42)
		{
			signal(SIGINT, sig);
 			ft_init_term(sh);
			if (ft_prompt(sh) > 0)
			{
				ft_restore_term();
				ft_parse(sh); // protect if parse error
				//ft_reinit_line(sh->line);
				ft_strdel(&sh->line->line); // save line->line, free all line struct
				ft_strdel(&sh->line->saved); // save line->line, free all line struct
				sh->line->cursor = 0;
				sh->line->len = 0;
				sh->line->key = 0;
				ft_putchar('\n');
				ft_execute(sh); // , line
				wait(0);
				ft_free_ptree(&sh->parse->root);
				free(sh->parse);
				sh->parse = NULL;
				ft_putstr(sh->prompt);
			}
		}
	return (0);
}

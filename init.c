/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/07 11:26:35 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 16:11:39 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
//#include "init.h"

int	ft_init_line(t_line *line, size_t prompt_len)
{
	line->cursor = 0;
	line->len = 0;
	line->key = 0;
	line->prev_key = 0;
	line->hist_i = 0;
	line->hist_n = 0;
	line->line = ft_strnew(1);
	line->p_len = prompt_len;
	line->saved = ft_strnew(1);
	if (!(line->key_funcs = ft_keys_list()))
		return (-1);
	return (0);
}

static char	**ft_builtins_tab()
{
	static char *builtins[NB_BUILTINS] = {
			"exit",
			"cd",
			"env",
			"setenv",
			"unsetenv",
			"echo"
						};

	return (builtins);
}

int		ft_init_basics(t_sh *sh)
{
	sh->builtins = ft_builtins_tab();
	if (!(sh->exec = (t_exec *)malloc(sizeof(t_exec))))
		return (-1);
	if (!(sh->line = (t_line *)malloc(sizeof(t_line))))
		return (-1);
	sh->line->line = NULL;
	return (0);
}

int		ft_init(t_sh *sh, char **envp)
{
	if (!(ft_get_prompt(&sh->prompt)))
		return (-1);
	if (ft_init_basics(sh) < 0)
		return (-1);
	if (ft_init_line(sh->line, ft_strlen(sh->prompt)) < 0)
		return (-1);
	if (!(sh->line->termcaps = (t_term *)malloc(sizeof(t_term))))
		return (-1); /* malloc error */

	ft_init_hist(sh->line);
	if (!(sh->exec->env = ft_list_env(envp)))
		return (-1);
	//ft_token_tab(sh);
	return (0);
}

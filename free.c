/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/24 16:31:34 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/24 16:32:48 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void ft_free_env(t_env **env)
{
	t_env *tmp;

	if (*env)
	{
		ft_strdel(&(*env)->var);
		if ((*env)->value)
			ft_strdel(&(*env)->value);
		tmp = (*env)->next;
		free(*env);
		*env = NULL;
		ft_free_env(&tmp);
	}
}

void	ft_free_cmd(t_command **cmd)
{
	/* free_array func in libft? */
	// ft_putendl("  ---------------  COMMAND BEEING FREED -------- ");
	ft_free_array(&(*cmd)->av);
	if ((*cmd)->path)
		ft_strdel(&(*cmd)->path);
	ft_strdel(&(*cmd)->name);
	free(*cmd);
	*cmd = NULL;
}

void	ft_free_exec(t_exec **exec)
{
	if ((*exec)->cmd)
		ft_free_cmd(&(*exec)->cmd);
	if ((*exec)->redir[0])
	{
		free(&(*exec)->redir[0]);
		(*exec)->redir[0] = NULL;
	}
	if ((*exec)->redir[1])
		free(&(*exec)->redir[1]);
	if ((*exec)->env)
		ft_free_env(&(*exec)->env);
	free(*exec);
	*exec = NULL;

}


   void		ft_free_hist(char **hist)
   {
		int i;

		i = 0;
		while (i < HISTSIZE && hist[i])
		{
			ft_strdel(&hist[i]);
			i++;
		}
   }

void	free_tokens(t_tkns **tokens)
{
	if (*tokens)
	{
		if ((*tokens)->next)
			free_tokens (&(*tokens)->next);
		ft_strdel(&(*tokens)->data);
		free(*tokens);
		*tokens = NULL;
	}
}

void	ft_free_ptree(t_parse_tree **root)
{
	if ((*root)->right)
		ft_free_ptree(&(*root)->right);
	if ((*root)->left)
		ft_free_ptree(&(*root)->left);
	if (*root)
	{
		free_tokens(&(*root)->token);
		free((*root));
		(*root) = NULL;
	}
}

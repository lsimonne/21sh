/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 14:55:36 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 15:24:49 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include "command_line.h"
#include "history.h"

int	ft_init_hist(t_line *line)
{
	int		fd;
	char	*hist;

	while (line->hist_i < HISTSIZE)
	{
		line->hist[line->hist_i] = NULL;
		line->hist_i++;
	}
	if ((fd = open(HISTFILE, O_RDWR	| O_CREAT, 0666)) < 0)
		return (-1);
	while (get_next_line(fd, &hist) > 0)
	{
		if (line->hist_n >= HISTSIZE)
			line->hist_n = 0;
		line->hist[line->hist_n] = ft_strdup(hist);
		line->hist_n++;
		ft_strdel(&hist);
	}
	close(fd);
	line->hist_i = line->hist_n;
	return (0);
}

int	ft_add_to_hist(t_line *line)
{
	int		fd;
	char	buf[BUFF_SIZE];

	if (line->line && ft_strcmp(line->line, "") != 0)
	{
		line->hist_i = 0;
		if ((fd = open(HISTFILE, O_RDWR)) < 0)
			return (-1);
		while (read(fd, buf, BUFF_SIZE) != 0)
			;
		ft_putendl_fd(line->line, fd);
		if (line->hist_n < HISTSIZE)
			line->hist_i = line->hist_n++;
		else
			line->hist_i = line->hist_n;
		line->hist[line->hist_n - 1] = ft_strdup(line->line);
		close(fd);
	}
	return (0);
}

int ft_up(t_line *line)
{
	//char	*to_find;

	if (line->prev_key != UP_KEY && line->prev_key != DOWN_KEY)
		line->hist_i = line->hist_n;
	if (line->line && *line->line)
	{
		//	to_find = ft_strdup(line->line);
		ft_strdel(&line->line);
	}
	while (line->cursor > 0)
	{
		if (tputs(line->termcaps->le, 1, ft_outc) < 0)
			return (-1);
		line->cursor--;
	}
	if (tputs(line->termcaps->ce, 1, ft_outc) < 0) // clear all screen instead of endofline
		return (-1);
	if (line->hist_i > 0)
	{
		line->hist_i--;
		line->line = ft_strdup(line->hist[line->hist_i]);
	}
	else
		line->line = ft_strdup("");
	line->cursor = ft_strlen(line->line);
	line->len = line->cursor;
	if (line->line)
		ft_putstr(line->line);
	return (0);
}

int ft_down(t_line *line)
{
	//char	*to_find;

	if (line->prev_key != UP_KEY && line->prev_key != DOWN_KEY)
		line->hist_i = line->hist_n - 1;
	if (line->line && *line->line)
	{
		//	to_find = ft_strdup(line->line);
		ft_strdel(&line->line);
	}
	while (line->cursor > 0) // func to clear line?
	{
		if (tputs(line->termcaps->le, 1, ft_outc) < 0)
			return (-1);
		line->cursor--;
	}
	if (tputs(line->termcaps->ce, 1, ft_outc) < 0) // clear all screen instead of endofline
		return (-1);
	if (line->hist_i < line->hist_n)
	{
		line->line = ft_strdup(line->hist[line->hist_i]);
		line->hist_i++;
	}
	else
		line->line = ft_strdup("");
	line->cursor = ft_strlen(line->line);
	line->len = line->cursor;
	if (line->line)
		ft_putstr(line->line);
	return (0);
}

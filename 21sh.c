/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   21sh.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 15:00:00 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 16:17:11 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include "command_line.h"

int		ft_prompt(t_sh *sh)
{
	char	*tmp;
	int		i;

		// sig winchange
		//signal(SIGINT, sig);
		if (read(0, &sh->line->key, sizeof(long)) <= 0 || sh->line->key == CTRL_D)
		{
			// free and exit function
			exit(0);
		}
		if ((i = ft_key_handler(sh->line, sh->line->key_funcs) > 0))
		{
			tmp = ft_strdup(sh->line->line);
			//if (sh->line->line && *sh->line->line)
				ft_strdel(&sh->line->line);
			sh->line->line = ft_strtrim(tmp);
			ft_strdel(&tmp);
			if (*sh->line->line)
				ft_add_to_hist(sh->line);
			else
			{
				ft_putchar('\n');
				ft_putstr(sh->prompt);
				return (0);
			}
		}
		sh->line->prev_key = sh->line->key;
		sh->line->key = 0;
		return (i);
}
				// funcs tab according to token
				/**
				precedence : && and || have the same precedence -> left-associative (EXCEPT if {} are taken into account), before ; and & which have equal precedence
				how to store the succession of commands WITH their logical links?
				 *********************/

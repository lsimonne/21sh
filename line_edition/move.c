/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 14:44:23 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/09 14:46:07 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include "command_line.h"

int ft_left(t_line *line)
{
	if (line->cursor > 0)
	{
		if (tputs(line->termcaps->le, 1, ft_outc) < 0)
			return (-1); // general term error
		line->cursor--;
	}
	if (line->cursor < line->len)
	{
		if (line->saved)
			ft_strdel(&line->saved);
		line->saved = ft_strsub(line->line, line->cursor, line->len - line->cursor);
	}
	return (0);
}

int ft_right(t_line *line)
{
	/* wraps to next line (left func) but doesn't work the other way (from left margin to right margin one line up) */
	if (line->cursor < line->len)
	{
		if (tputs(line->termcaps->nd, 1, ft_outc) < 0)
			return (-1); // general term error
		line->cursor++;
	}
	if (line->cursor < line->len)
	{
		if (line->saved)
			ft_strdel(&line->saved);
		line->saved = ft_strsub(line->line, line->cursor - 1, line->len - line->cursor + 1);
	}
	return (0);
}

int	ft_home(t_line *line)
{
	while (line->cursor > 0)
	{
		if (tputs(line->termcaps->le, 1, ft_outc) < 0)
			return (-1);
		line->cursor--;
	}
	/***
	func save_rest_line*/
	if (line->cursor < line->len)
	{
		if (line->saved)
			ft_strdel(&line->saved);
		line->saved = ft_strsub(line->line, line->cursor, line->len - line->cursor);
	}
	/************************/
	return (0);
}

int	ft_end(t_line *line)
{
	while (line->cursor < line->len)
	{
		if (tputs(line->termcaps->nd, 1, ft_outc) < 0)
			return (-1);
		line->cursor++;
	}
	if (line->saved)
		ft_strdel(&line->saved);
	return (0);
}

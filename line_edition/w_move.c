#include "shell.h"
#include "command_line.h"

int	ft_word_left(t_line *line)
{
	while (line->cursor > 0 && line->line[line->cursor] != ' ')
	{
		if (tputs(line->termcaps->le, 1, ft_outc) < 0)
			return (-1);
		line->cursor--;
	}
	while (line->cursor > 0 && line->line[line->cursor] == ' ')
	{
		line->cursor--;
	}
	if (line->cursor < line->len) // make function save (used in move.c also)
	{
		if (line->saved)
			ft_strdel(&line->saved);
		line->saved = ft_strsub(line->line, line->cursor, line->len - line->cursor);
	}
	return (0);
}


/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcaps.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 15:57:48 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/09 12:13:08 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command_line.h"
#include "shell.h"

int		ft_outc(int c)
{
	write(1, &c, 1);
	return (0);
}

int		ft_get_caps(t_term *termcaps)
{
	if (!(termcaps->le = tgetstr("le", NULL)))
		return (-1);
	if (!(termcaps->nd = tgetstr("nd", NULL)))
		return (-1);
	if (!(termcaps->ce = tgetstr("ce", NULL)))
		return (-1);
	if (!(termcaps->sc = tgetstr("sc", NULL)))
		return (-1);
	if (!(termcaps->rc = tgetstr("rc", NULL)))
		return (-1);
	if (!(termcaps->dc = tgetstr("dc", NULL)))
		return (-1);
	if (!(termcaps->im = tgetstr("im", NULL)))
		return (-1);
		/*

	if (!(termcaps-> = tgetstr("", NULL)))
		return (-1);
	if (!(termcaps-> = tgetstr("", NULL)))
		return (-1);*/
	/*if (!(termcaps->ve = tgetstr("ve", NULL)))
		return (-1);*/
	return (0);
}

int		ft_raw_term(void)
{
	char			*term_name;
	struct termios	term;

	if (!(term_name = getenv("TERM")))
	{
		ft_putendl("\033[31m21sh: command line edition will not be handled as the program was unable to find a TERM variable\n\033[0m"); // define colors
		return (-1); // return (error handler)
	}
	if (tgetent(0, term_name) == -1)
		ft_putendl("getent error");
	tcgetattr(0, &term);
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0; // retour de read tous les n delai
	if (tcsetattr(0, TCSANOW, &term) == -1)
		ft_putendl("tcssetattr error");
	return (0);
}

void 	ft_init_term(t_sh *sh)
{
	if (ft_raw_term() < 0 || ft_get_caps(sh->line->termcaps) < 0)
	{
		free(sh->line->termcaps);
		sh->line->termcaps = NULL;
		ft_putendl("no termcaps");
		exit (-1);
	}
}

int     ft_restore_term()
{
	struct termios  term;
	// restore saved term instead

	tcgetattr(0, &term);
	term.c_lflag = (ICANON | ECHO); // mode interactif i.e. resultat de read a chaque touche enfoncee
	if (tcsetattr(0, TCSANOW, &term) == -1)
		ft_putendl("tcssetattr error");
	/*
	if (tputs(tgetstr("te", NULL), 1, ft_outc) == -1)
		return (-1);*/
	return (0);
}

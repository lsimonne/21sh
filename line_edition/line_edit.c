/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_edit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 14:59:43 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 16:17:56 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include "command_line.h"

int	ft_ret(t_line *line)
{
	line->key = 0;
	return (1);
}

int     ft_add_char(t_line *line)
{
	char    *tmp;
	char 	*tmp2;

	if (line->cursor < line->len)
	{
		tputs(line->termcaps->ce, 1, ft_outc);
		tputs(line->termcaps->sc, 1, ft_outc);
		ft_putstr(line->saved);
		tputs(line->termcaps->rc, 1, ft_outc);
		tmp = ft_strsub(line->line, 0, line->cursor - 1);
		ft_strdel(&line->line);
		tmp2 = ft_strjoin(tmp, (char *)&line->key);
		line->line = ft_strjoin(tmp2, line->saved);
		ft_strdel(&tmp);
		ft_strdel(&tmp2);
	}
	else
	{
		if (line->line)
		{
			tmp = ft_strdup(line->line);
			ft_strdel(&line->line);
			line->line = ft_strjoin(tmp, (char *)&line->key);
			ft_strdel(&tmp);
		}
		else
			line->line = ft_strdup((char *)&line->key);
	}
	return (0);
}

int	ft_del(t_line *line)
{
	char	*tmp;

	if (line->cursor > 0)
	{
		line->cursor--;
		line->len--;
		if (tputs(line->termcaps->le, 1, ft_outc) < 0)
			return (-1);
		if (tputs(line->termcaps->dc, 1, ft_outc) < 0)
			return (-1);
		if (line->saved)
		{
			tmp = ft_strsub(line->line, 0, line->cursor);
			ft_strdel(&line->line);
			line->line = ft_strjoin(tmp, line->saved);
			ft_strdel(&tmp);
		}
		else
		{
			tmp = ft_strsub(line->line, 0, line->len);
			ft_strdel(&line->line);
			line->line = ft_strdup(tmp);
			ft_strdel(&tmp);
		}
		ft_strdel(&tmp);
	}
	return (0);
}

int     ft_key_handler(t_line *line, t_keys *key_funcs)
{
	int	i;

	i = 0;
	if (line->cursor == line->len && line->saved)
	{
		//ft_putendl("saved deleted");
		ft_strdel(&line->saved);
	}
	//ft_putlnbr(line->key);
	// table of structures instead of list ?
	/*while (i < 8)
	{
		if (key_funcs[i].key == line->key)
			return (key_funcs[i].f(line));
		i++;
	}*/
	while (key_funcs)
	{
		if (key_funcs->key == line->key)
		{
			//ft_putendl("\nfound key func in line_edit");
			return (key_funcs->f(line));
		}
		key_funcs = key_funcs->next;
	}
	if ((line->key = line->key & 255) > 31 && line->key < 127) // ft_valid_char()
	{
		line->len++;
		line->cursor++;
		ft_putchar(line->key);
		ft_add_char(line);
		line->prev_key = line->key;
	}
	return (0);
}

t_keys	*ft_new_link(t_keys **beg, int key, int(*f)(t_line *line))
{
	t_keys	*new_link;

	if (!(new_link = (t_keys *)malloc(sizeof(t_keys))))
		return (NULL); //error
	new_link->key = key;
	new_link->f = f;
	new_link->next = *beg;
	return (new_link);
}

t_keys	*ft_keys_list(void)
{
	t_keys	*key_funcs;
	int		i;
	long	keys[] = {LEFT_KEY, RIGHT_KEY, HOME_KEY, END_KEY, UP_KEY, DOWN_KEY, RET_KEY, DEL_KEY, OPT_LEFT};
	int	(*f[])(t_line *line) = {ft_left, ft_right, ft_home, ft_end, ft_up, ft_down, ft_ret, ft_del, ft_word_left};

	i = 0;
	key_funcs = NULL;
	while (i < 9)
	{
		key_funcs = ft_new_link(&key_funcs, keys[i], f[i]);
		i++;
	}
	return (key_funcs);
}

#include "shell.h"

int      ft_init_redir(t_exec *exec)
{
	if (!(exec->redir[0] = (t_redir *)ft_memalloc(sizeof(t_redir))))
		exit(-1);
	if (!(exec->redir[1] = (t_redir *)ft_memalloc(sizeof(t_redir))))
		exit(-1); // malloc error -> exit?
	exec->redir[0]->type = -1;
	exec->redir[0]->to_redir = 1;
	exec->redir[0]->fd_out = -1;
	exec->redir[1]->type = -1;
	exec->redir[1]->to_redir = -1;
	exec->redir[1]->fd_out = 0;
	return (0);
}

static int         ft_redir_type(char *token)
{
	char *redirs[] = {">", ">>", "<", "<<"};
	int i;

	i = 0;
	while (i < 4)
	{
		if (ft_strcmp(token, redirs[i]) == 0)
			return (i);
		i++;
	}
	return (-1);
}

static int         ft_agreg_type(char *token)
{
	char *ag[] = {">&", "<&"};
	int i;

	i = 0;
	while (i < 2)
	{
		if (ft_strcmp(token, ag[i]) == 0)
			return (i);
		i++;
	}
	return (-1);
}

static int			ft_choose_open(t_exec *exec, t_tkns *token, int type, int n)
{
	// function pointers?
	if (type == 0 || type == 1)
	{
		if (n > 0)
			exec->redir[0]->to_redir = n;
		if ((exec->redir[0]->fd_out = (!type) ? open(token->data, O_RDWR | O_CREAT | O_TRUNC, 0666) \
				: open(token->data, O_RDWR | O_CREAT | O_APPEND, 0666)) < 0)// check if exists as dir?
			return (-1); // should erase existing file
	}
	else if (type == 2)
	{
		if (n > 0)
			exec->redir[1]->fd_out = n;
		if ((exec->redir[1]->to_redir = open(token->data, O_RDONLY)) < 0)
			return (-1);
	}
	else // HEREDOC TO IMPLEMENT
	{
		if (n < 0)
			n = 0;
		exec->redir[1]->type = -2; // temporary
		ft_start_hdoc(token->data, n);
	}
	return (0);
}

int					ft_set_redir(t_exec *exec, t_tkns **tokens)
{
	int		type;
	int		fd;

	if (ft_isnum((*tokens)->data))
	{
		fd = ft_atoi((*tokens)->data);
		(*tokens) = (*tokens)->next;
	}
	else
		fd = -1;
	if ((type = ft_redir_type((*tokens)->data)) >= 0)
	{
		if (((*tokens) = (*tokens)->next))
			ft_choose_open(exec, *tokens, type, fd);
		else
			ft_putendl("parse_error"); // error func return
	}
	else if ((type = ft_agreg_type((*tokens)->data)) >= 0)
	{
		if (((*tokens) = (*tokens)->next))
		{
			if (!ft_isnum((*tokens)->data))
				return (-1); // error
			if (type == 0)
				exec->redir[0]->to_redir = ft_atoi((*tokens)->data);
		}
	}
	return (0);
}

int		ft_handle_redir(t_exec *exec)
{
	if (exec->redir[0] && exec->redir[0]->fd_out > 0) // redirection of output: type 0
	{
		ft_putendl("redirecting output");
			exec->redir[0]->type = 1;
			exec->saved_stdout = dup(exec->redir[0]->to_redir);
			dup2(exec->redir[0]->fd_out, exec->redir[0]->to_redir);
			close(exec->redir[0]->fd_out);

	}
	if (exec->redir[1] && exec->redir[1]->to_redir > 0) // input redir
	{
			exec->redir[1]->type = 1;
			exec->saved_stdin = dup(exec->redir[1]->fd_out);
			dup2(exec->redir[1]->to_redir, exec->redir[1]->fd_out);
			close(exec->redir[1]->to_redir);
	}
	return (0);
}

#include "typedefs.h"
#include <stdlib.h>
#include <unistd.h>

void			close_redirs(t_exec *exec)
{
		if (exec->redir[0] && exec->redir[0]->type > 0)
		{
				dup2(exec->saved_stdout, STDOUT_FILENO);
				close(exec->saved_stdout);
		}
		if (exec->redir[1] && exec->redir[1]->type > 0)
		{
				dup2(exec->saved_stdin, STDIN_FILENO);
				close(exec->saved_stdin);
		}
}

void			free_redirs(t_exec *exec)
{
	free(exec->redir[0]);
	exec->redir[0] = NULL;
	free(exec->redir[1]);
	exec->redir[1] = NULL;
}
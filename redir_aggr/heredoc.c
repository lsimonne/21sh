#include "shell.h"
/*
int		ft_add_link(char *data, t_hdoc *heredoc)
{
	t_hdoc	*tmp;

	tmp = heredoc;
	if (heredoc)
	{
		while (tmp)
			tmp = tmp->next;
		if (!(tmp->next = (t_hdoc *)malloc(sizeof(t_hdoc))))
			return (-1); //malloc error
		tmp = tmp->next;
	}
	else
	{
		if (!(heredoc = (t_hdoc *)malloc(sizeof(t_hdoc))))
			return (-1);
	}
	tmp->line = ft_strdup(data);
	tmp->next = NULL;
	return (0);
}*/

int		ft_start_hdoc(char *delimiter, int fd)
{
	char		*line;
	char		*heredoc;
	char		*tmp;
	int			pipefd[2];
	int			process;

	ft_putstr("> ");
	fd = 2;
	heredoc = ft_strnew(0);
	while (get_next_line(0, &line) > 0 && ft_strcmp(line, delimiter))
	{
			ft_putstr("> ");
			tmp = ft_strjoin(heredoc, "\n");
			ft_strdel(&heredoc);
			heredoc = ft_strjoin(tmp, line);
			ft_strdel(&tmp);
			ft_strdel(&line);
	}

	pipe(pipefd);
	process = fork();
	if (process == 0)
	{
		close(pipefd[0]);
		ft_putstr_fd(heredoc, pipefd[1]);
		exit(0);
	}
	if (process > 0)
	{
		;//
	}
	// TO DO (?)
	return (0);

}

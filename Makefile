NAME = 21sh

HEADER = 	includes/shell.h \
			includes/typedefs.h \
			includes/command_line.h \
			includes/history.h \
			includes/parser.h

FLAGS = -Wall -Werror -Wextra

LIB = libft

LINE_EDIT = line_edition/move.c \
			line_edition/w_move.c \
			line_edition/line_edit.c \
			line_edition/termcaps.c

BUILTINS = 	builtins/builtins.c \
			builtins/exit.c \
			builtins/cd.c \
			builtins/cd2.c \
			builtins/env.c \
			builtins/env2.c \
			builtins/env3.c \
			builtins/echo.c \
			builtins/errors.c

PARSER_LEXER =	parser/parse.c \
				parser/tokenizer.c \
				parser/errors.c \
				parser/tools.c

EXEC = 	redir_aggr/redir.c\
		redir_aggr/heredoc.c \
		redir_aggr/close_free_redirs.c

SRC = 	main.c \
		errors.c \
		init.c \
		history.c \
		prompt.c \
		21sh.c \
		command.c \
		execute.c \
		free.c \
		signals.c \
		wait.c \
		pipe.c \
		debug.c

ALL_SRC = $(SRC) $(LINE_EDIT) $(BUILTINS) $(PARSER_LEXER) $(EXEC)

OBJ = 	$(ALL_SRC:%.c=obj/%.o) \

all: prep compile_lib $(NAME)

prep:
	mkdir -p obj/ obj/line_edition obj/builtins obj/parser obj/redir_aggr

compile_lib:
			make -C $(LIB)/

$(NAME): $(OBJ)
		gcc $(FLAGS) -o $(NAME) $(OBJ) -L $(LIB)/ -lft -ltermcap

obj/%.o: %.c $(HEADER)
		gcc $(FLAGS) -I $(LIB)/includes -I includes/ -o $@ -c $<

clean:
		make -C $(LIB)/ fclean
		rm -rf -- obj/

fclean: clean
		rm -f $(NAME)
		make -C $(LIB)/ fclean

re: fclean all

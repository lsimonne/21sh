#include "shell.h"

void	ft_signals(int signal)
{
	char	*prompt;

	if (signal == SIGINT)
	{
		ft_putchar('\n');
		ft_get_prompt(&prompt);
		ft_putstr(prompt);
	}
}

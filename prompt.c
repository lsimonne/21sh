/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/07 13:18:26 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 15:23:48 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include <sys/utsname.h>

static char *ft_get_user(char *tmp)
{
	char    *user;
	char    *prompt;

	user = NULL;
	user = getenv("USER");
	if (user)
		prompt = ft_strjoin(tmp, user);
	else
		prompt = ft_strjoin(tmp, "anonymous");
	return (prompt);
}

int 	ft_get_prompt(char **prompt)
{
	char            *tmp;
	int             len;
	struct utsname  machine;

	if (uname(&machine) != -1)
	{
		tmp = ft_strjoin("[", machine.nodename);
		if (ft_strchr(tmp, '.'))
		{
			len = ft_strchr(tmp, '.') - tmp;
			*prompt = ft_strsub(tmp, 0, len);
		}
		else
			*prompt = ft_strdup(tmp);
		ft_strdel(&tmp);
		tmp = ft_strjoin(*prompt, " - ");
		ft_strdel(prompt);
		*prompt = ft_get_user(tmp);
		ft_strdel(&tmp);
		tmp = ft_strjoin(*prompt, "]  $> ");
		ft_strdel(prompt);
		*prompt = ft_strdup(tmp);
		ft_strdel(&tmp);
	}
	return (1);   // protect everything -> errors
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 15:03:48 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 03:37:17 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

int     ft_token_type(char *data, bool type)
{
	int     j;
	int     i;
	char    *c_ops[] = {"|", ";"};
	char	*redir[] = {">", ">>", "<", "<<"}; // << heredoc
	char	*aggr[] = {"<&", ">&"};
	// + <> op

	j = 0;
	i = 0;
	if (type == 0)
		return (0);
	while (j < 2)
	{
		if (ft_strcmp(c_ops[j], data) == 0)
			return (TK_CTRLOP);
		else if (ft_strcmp(aggr[j], data) == 0)
			return (TK_AGGR);
		else if (ft_strcmp(redir[j], data) == 0)
			return (TK_REDIR);
		j++;
	}
	while (j < 4)
	{
		if (ft_strcmp(redir[j], data) == 0)
			return (TK_REDIR);
		j++;
	}
	return (-1);
}

int			ft_add_token(t_parse *parse, char *data, bool type) // macro for types?
{
	t_tkns	*new_token;

	if (!(new_token = (t_tkns *)malloc(sizeof(t_tkns))))
		return (-1); // malloc error
	new_token->data = ft_strtrim(data);
	ft_strdel(&data);
	new_token->type = ft_token_type(new_token->data, type);
	new_token->nb_tkns = 1;
	new_token->next = NULL;
	ft_add_to_tree(parse, new_token);
	return (0);
}

// control operators :  ‘||’, ‘&&’, ‘&’, ‘;’, ‘;;’, ‘|’,   {   ‘|&’, ‘(’,  ‘)’     }
// metacharacters ‘|’, ‘&’, ‘;’, ‘(’, ‘)’, ‘<’, ‘>’
// quotes && backslash '' "" () {} [] '\'

void		ft_quotes(char c, t_parse *parse)
{
	char	quotes[] = {39, 34, 92};
	int		i;

	i = 0;
	while (i < 3)
	{
		if (c == quotes[i])
		{
			if (parse->curr_quote != NO_QU && parse->curr_quote == i)
				parse->curr_quote = NO_QU;
			else
				parse->curr_quote = i;
			return;
		}
		i++;
	}
	return;
}

int     ft_tokenizer(t_sh *sh, char *line)
{
	int     i;

	// if !*sh->line->line eoi as token

	while (line && *(line))
	{
		while (ft_is_blank(*(line))) // & unquoted
			(line)++;
		i = 0;
		if (*line && !ft_is_metachar(*(line)))
		{
			while (line[i] && ((!(ft_is_metachar(line[i])) && !ft_is_blank(line[i])) || sh->parse->curr_quote != NO_QU)) // ft_continue_word
			{
				ft_quotes(line[i], sh->parse);
				i++;
			}
			// check if isnum && no blank between num & metachar
			ft_add_token(sh->parse, ft_strsub(line, 0, i), 0);
			line += i;
			sh->parse->curr_quote = NO_QU;
		}
		else
		{
			while (ft_is_metachar(line[i]))
			{
				if (i > 2)
					return (ft_token_error(line[i - 1]));
				i++;
			}

			ft_add_token(sh->parse, ft_strsub(line, 0, i), 1);
			line += i;
		}
	}

	/******  debug
	  t_tkns *tmp = sh->parse->tokens;
	  ft_putendl("\n---------- TOKENIZER DEBUG");
	  while (tmp)
	  {
	  ft_putstr(tmp->data);
	  ft_putstr(", ");
	  ft_putnbr(tmp->type);
	  ft_putchar('\n');
	  tmp=tmp->next;
	  }
	*/
	return (0);
}
/*
// handle quotes : a single quote cannot occur within single quotes  i.e. ''' / double quotes don't work for backslash
*/

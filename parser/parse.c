/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/07 15:08:45 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 15:13:05 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

t_parse_tree	*ft_new_root()
{
	t_parse_tree *new_root;

	if (!(new_root = (t_parse_tree *)malloc(sizeof(t_parse_tree))))
		return (NULL); // malloc error
	new_root->parent = NULL;
	new_root->token = NULL;
	new_root->left = NULL;
	new_root->right = NULL;
	return (new_root);
}

t_parse_tree	*ft_new_node()
{
	t_parse_tree *new_node;

	if (!(new_node = (t_parse_tree *)malloc(sizeof(t_parse_tree))))
		return (NULL); // malloc error
	new_node->left = NULL;
	new_node->right = NULL;
	new_node->parent = NULL;
	return (new_node);
}

static void	ft_append_token(t_tkns *token, t_tkns **tok_list)
{
	t_tkns *tmp;

	tmp = *tok_list;
	(*tok_list)->nb_tkns++;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = token;
}

int		ft_add_to_tree(t_parse *parse, t_tkns *new_token)
{
	t_tkns *tmp;

	if (new_token->type == TK_CTRLOP)
	{
		if (!parse->ptree->token && ft_strcmp(new_token->data, ";"))
		{
			return (-1);
			// errors : && without anthing before
			// parse->ptree->token = new_token;
		}
		else
		{
			tmp = parse->ptree->token;
			parse->ptree->token = new_token; // del + change token
			parse->ptree->left = ft_new_node();
			parse->ptree->right = ft_new_node();
			parse->ptree->right->parent = parse->ptree;
			parse->ptree->left->token = tmp;
			parse->ptree->left->parent = parse->ptree;
		}
		// error case if no right operand
	}
	else
	{
		if (parse->ptree->token)
		{
			if (parse->ptree->token->type == TK_CTRLOP)
			{
				parse->ptree = parse->ptree->right;
				parse->ptree->token = new_token;
			}
			else
				ft_append_token(new_token, &parse->ptree->token);
		}
		else
			parse->ptree->token = new_token;
	}
	return (0);
}

int			ft_parse(t_sh *sh)
{
	if (!(sh->parse = (t_parse *)ft_memalloc(sizeof(t_parse))))
		return (-1);
	sh->parse->root = ft_new_root();
	sh->parse->ptree = sh->parse->root;
	sh->parse->curr_quote = NO_QU;
	if (ft_tokenizer(sh, sh->line->line) < 0)
		ft_putendl("tokenizer error");
	while (sh->parse->ptree && sh->parse->ptree->parent)
		sh->parse->ptree = sh->parse->ptree->parent;
	/*debug

	t_parse_tree *tmp2;

	tmp2 = sh->parse->root;
	while (tmp2)
	{
		if (tmp2->token)
		{
			while (tmp2->token)
			{
				ft_putendl(tmp2->token->data);
				tmp2->token = tmp2->token->next;
			}
		}
		if (tmp2->left)
		{
			ft_putstr("left: ");
			while (tmp2->left->token)
			{
				ft_putendl(tmp2->left->token->data);
				tmp2->left->token = tmp2->left->token->next;
			}
		}
		if (tmp2->right)
		{
			ft_putstr("right: ");
			while (tmp2->left->token)
			{
				ft_putendl(tmp2->left->token->data);
				tmp2->left->token = tmp2->left->token->next;
			}
		}
		tmp2 = tmp2->right;
	}
	ft_putendl("end of debug");

*/


	// separate into simple commands
	// define order of execution
	// parse commands (take out quotes, no trimming after that i.e. if "ls     " command is not found)
	return (0);
}

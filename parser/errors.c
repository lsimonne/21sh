/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 10:48:43 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/28 10:51:43 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int			ft_token_error(char c)
{
	ft_putstr("21sh: syntax error near unexpected token '");
	ft_putchar(c);
	ft_putendl("'");
	return (-1);
}

#include "shell.h"

void	debug_redir(t_redir **redir)
{
	ft_putendl("\n-----------------\nREDIR OUTPUT: ");
    if (redir[0])
    {
    ft_putstr("to_redir 0: ");  
	   ft_putnbr(redir[0]->to_redir);ft_putchar('\n');
	ft_putstr("fd_out: ");
	ft_putnbr(redir[0]->fd_out);ft_putchar('\n');
	ft_putendl("\n-----------------\nREDIR INPUT: ");
	}
    if (redir[1])
    {
        ft_putstr("to_redir 1: ");
	   ft_putnbr(redir[1]->to_redir);ft_putchar('\n');
	ft_putstr("fd_out: ");
	ft_putnbr(redir[1]->fd_out);ft_putchar('\n');
    }
}

void	debug_tokens(t_tkns *tokens)
{
	ft_putendl("\n-----------------\nTOKENS: ");
	while (tokens)
	{
		ft_putstr("type:");
		ft_putnbr(tokens->type);ft_putchar('\n');
		ft_putstr("data: ");
		ft_putendl(tokens->data);
		tokens = tokens->next;
	}
}

void    debug_init(t_sh *sh)
{
    ft_putendl("PROMPT: ");
    ft_putendl(sh->prompt);
    ft_putchar('\n');
    ft_putendl("BUILTINS: ");
    ft_display_array(sh->builtins);
    ft_putchar('\n');
    ft_putendl("ENV: ");
    ft_print_env(sh->exec->env);
    ft_putendl("LINE key: ");
    ft_putnbr(sh->line->key);
    ft_putchar('\n');
    ft_putendl("LINE line: ");
    ft_putendl(sh->line->line);
    ft_putendl("LINE p_len: ");
    ft_putnbr(sh->line->p_len);
    ft_putchar('\n');
    ft_putendl("LINE termcaps: ");
    ft_putendl(sh->line->termcaps->le);
	ft_putendl("HISTORY: ");
	ft_display_array(sh->line->hist);
	ft_putchar('\n');
}

void	debug_hist(t_line *line)
{
	ft_putstr("hist_i: ");
	ft_putnbr(line->hist_i);ft_putchar('\n');
	ft_putstr("hist_n: ");
	ft_putnbr(line->hist_n);ft_putchar('\n');

}

void	debug_command(t_command *cmd)
{
    ft_putstr("command path: ");
    ft_putendl(cmd->path);
    ft_putstr("command name: ");
    ft_putendl(cmd->name);
    ft_putstr("command arg count: ");
    ft_putnbr(cmd->ac);
	ft_putchar('\n');
    ft_putstr("command arguments: ");
    ft_display_array(cmd->av);
    ft_putchar('\n');
}

void        debug_line(t_line *line)
{
    ft_putchar('\n');
    ft_putstr("line: ");
    ft_putendl(line->line);
    ft_putstr("saved: ");
    if (line->saved)
        ft_putendl(line->saved);
    ft_putstr("len: ");
    ft_putnbr(line->len);ft_putchar('\n');
    ft_putstr("cursor: ");
    ft_putnbr(line->cursor);ft_putchar('\n');
}

void		display_quote_enum()
{
	 ft_putstr("no_qu: ");
     ft_putnbr(NO_QU);
     ft_putchar('\n');
     ft_putstr("simple qu: ");
     ft_putnbr(QU_SIMPLE);
     ft_putchar('\n');
     ft_putstr("double qu: ");
     ft_putnbr(QU_DOUBLE);
     ft_putchar('\n');
}

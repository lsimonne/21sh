/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/07 14:59:51 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/24 16:46:14 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int     ft_wrong_command(int code, t_command **cmd)
{
	if ((*cmd)->name)
	{
		ft_putstr("21sh: ");
		ft_putstr_fd((*cmd)->name, 2);
		if (code == -1)
			ft_putendl_fd(": command not found", 2);
		else
			ft_putendl_fd(": Permission denied", 2);
	}
	ft_free_cmd(cmd);
	return (-1);
}

int		ft_2many_args(char *command)
{
	ft_putstr_fd(command, 2);
	ft_putendl_fd(": Too many arguments", 2);
	return (-1);
}

/*
int     ft_arg_error(int code, char *arg)
{
    ft_putstr_fd("21sh: ", 2);
    ft_putstr_fd(arg, 2);
    if (code == 1)
        ft_putendl_fd(": No such file or directory", 2);
    else if (code == 2)
        ft_putendl_fd(": Permission denied", 2);
    else if (code == 3)
        ft_putendl_fd(": is a directory", 2);
    else if (code == 4)
        ft_putendl_fd(": could not execute binary", 2);
    return (127);
}
*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 16:46:41 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 04:02:56 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		execute_leaf(t_sh *sh)
{
	pid_t process;

	process = -1;
	if ((sh->exec->cmd->type = ft_get_cmd(sh, sh->exec->cmd)) < 0)
		return (ft_wrong_command(sh->exec->cmd->type, &sh->exec->cmd));
	ft_handle_redir(sh->exec);
	if (sh->exec->cmd->type >= NB_BUILTINS)
		process = fork();
	else if (sh->exec->cmd->type >= 0 && sh->exec->cmd->type < NB_BUILTINS)
	{
		ft_builtins(sh);
		free_redirs(sh->exec);
		ft_free_cmd(&sh->exec->cmd);
	}
	if (process == 0)
		execve(sh->exec->cmd->path, sh->exec->cmd->av, ft_env_array(sh->exec->env));
	if (process > 0)
	{
		signal(SIGINT, SIG_IGN);
		wait_pids(sh->exec->to_wait, sh->exec->nb_pipes);
		wait(0);
		close_redirs(sh->exec);
		free_redirs(sh->exec);
		ft_free_cmd(&sh->exec->cmd);
	}
	return (0);
}

int 	execute_pipe_node(t_sh *sh)
{
	pid_t process;

	process = -1;
	sh->parse->ptree = sh->parse->ptree->left;
	if ((sh->exec->cmd->type = ft_get_cmd(sh, sh->exec->cmd)) < 0)
		 return (ft_wrong_command(sh->exec->cmd->type, &sh->exec->cmd));
	  if (sh->exec->cmd->type == NB_BUILTINS)
	  	process = fork();
	else if (sh->exec->cmd->type > 0 && sh->exec->cmd->type < NB_BUILTINS)
	{
	 	ft_builtins(sh);
		free_redirs(sh->exec);
		ft_free_cmd(&sh->exec->cmd);
	}
	 if (process == 0)
	 {
		sh->parse->ptree = sh->parse->ptree->parent;
		if (ft_run_pipe(sh) < 0)
			ft_putendl("run_pipe error");
	}
	  if (process > 0)
	 	wait(0);
	 return (0);
}

int		ft_execute(t_sh *sh)
{
	sh->exec->curr_stdin = 0;
	sh->exec->curr_stdout = 1;
	sh->exec->nb_pipes = 0;
	sh->exec->cmd = (t_command *)ft_memalloc(sizeof(t_command));
	if (!ft_strcmp(sh->parse->ptree->token->data, "|"))
		execute_pipe_node(sh);
	else if (!ft_strcmp(sh->parse->ptree->token->data, ";"))
	{
		sh->parse->ptree = sh->parse->ptree->left;
		if (sh->parse->ptree->token)
			ft_execute(sh);
		sh->parse->ptree = sh->parse->ptree->parent->right;
		ft_execute(sh);
	}
	else
		return (execute_leaf(sh));
	return (0);
}

#include "typedefs.h"
#include "shell.h"

static int		ft_exec(t_command *cmd, t_sh *sh) // exec struct only
{
	ft_handle_redir(sh->exec);
	if (cmd->type == NB_BUILTINS)
	{
		if (execve(cmd->path, cmd->av, ft_env_array(sh->exec->env)) < 0)
			return (-1); // failure return value?
	}
	else
		ft_builtins(sh);
	return (1);
}

static int 	pipe_parent(t_sh *sh, pid_t process)
{
	signal(SIGINT, SIG_IGN);
	sh->exec->nb_pipes++;
	ft_save_pid(sh->exec, process, sh->exec->nb_pipes);
	close(sh->exec->fildes[1]);
	dup2(sh->exec->fildes[0], STDIN_FILENO);
	sh->parse->ptree = sh->parse->ptree->right;
	ft_free_cmd(&sh->exec->cmd);
	free_redirs(sh->exec);
	ft_execute(sh);
	wait(0);
	exit(1);
}

int		ft_run_pipe(t_sh *sh)
{
	pid_t	process;

	process = -1;
	if (pipe(sh->exec->fildes) < 0)
		ft_putendl("pipe error"); // error func
	process = fork();
	if (process < 0)
		ft_putendl("insert process err func");
	if (process == 0)
	{
		close(sh->exec->fildes[0]);
		dup2(sh->exec->fildes[1], STDOUT_FILENO);
		ft_exec(sh->exec->cmd, sh);
	}
	if (process > 0)
		pipe_parent(sh, process);
	return (0);
}
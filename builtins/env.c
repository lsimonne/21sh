/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/10 11:13:47 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 04:00:09 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_env	*ft_env_copy(t_env *env)
{
	t_env	*copy;
	char	*elem;
	char	*tmp;

	copy = NULL;
	while (env)
	{
		tmp = ft_strjoin(env->var, "=");
		if (env->value)
			elem = ft_strjoin(tmp, env->value);
		else
			elem = ft_strdup(tmp);
		ft_strdel(&tmp);
		ft_add_var(elem, &copy);
		ft_strdel(&elem);
		env = env->next;
	}
	return (copy); // return int
}

int		ft_parse_options(char **args, t_env_opt *options)
{
	int		i;
	int		j;

	i = 1;
	while (args[i] && *args[i] == '-')
	{
		j = 1;
		if (args[i][j] == '-')
			break ;
		while (args[i][j])
		{
			if (args[i][j] == 'i')
				options->i = 1;
			else
				return (ft_env_opt_err(args[i][j]));
			j++;
		}
		i++;
	}
	return (0);
}

char	*ft_join_args(char **args, int i)
{
	char	*tmp;
	char	*tmp2;

	if (!(args[i + 1]))
		return (ft_strdup(args[i]));
	tmp = ft_strdup(args[i]);
	while (args[i + 1])
	{
		tmp2 = ft_strjoin(tmp, " ");
		i++;
		ft_strdel(&tmp);
		tmp = ft_strjoin(tmp2, args[i]);
		ft_strdel(&tmp2);
	}
	return (tmp);
}

int		ft_env(t_exec *exec)
{
	int			j;
	t_env_opt	opt;
	t_sh		*sh;

	j = 1;
	opt.i = 0;
	sh = NULL;
	if (exec->cmd->ac == 1)
		ft_print_env(exec->env);
	else
	{
		if (ft_parse_options(exec->cmd->av, &opt) == -1)
			return (-1); // ft_error
			/** initialize new sh **/
		if (!(sh = (t_sh *)malloc(sizeof(t_sh))) || ft_init_basics(sh) < 0)
			return (-1); // malloc error
		if (!(sh->exec->env = ft_env_copy(exec->env)))
			return (-1);
		sh->exec->cmd = NULL;
			/******/
		//if (opt.i == 1)
		//	ft_add_var("BUILTINS=exit:env:setenv:unsetenv:cd", &sh->exec->env); // add other vars?
		while (exec->cmd->av[j] && *exec->cmd->av[j] && *exec->cmd->av[j] == '-' \
				&& (!exec->cmd->av[j][1] || exec->cmd->av[j][1] != '-'))
			j++;
		ft_sub_env(exec, sh, j);
		/* free sh */
		if (sh->line->line && *sh->line->line)
			ft_strdel(&sh->line->line);
		free(sh->line);
		ft_free_exec(&sh->exec);
		free(sh);
		sh = NULL;
	}
	return (0);
}

char	**ft_env_array(t_env *env)
{
	char	**envp;
	char	*tmp2;
	int		size;
	t_env	*tmp;

	size = 0;
	tmp = env;
	while (tmp)
	{
		size++;
		tmp = tmp->next;
	}
	if (!(envp = (char **)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	envp[size] = NULL;
	size = 0;
	while (env)
	{
		tmp2 = ft_strjoin(env->var, "=");
		if (env->value)
			envp[size++] = ft_strjoin(tmp2, env->value);
		else
			envp[size++] = ft_strdup(tmp2);
		ft_strdel(&tmp2);
		env = env->next;
	}
	return (envp);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 17:34:46 by lsimonne          #+#    #+#             */
/*   Updated: 2016/08/31 16:25:44 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell.h>

int		ft_replace_var(t_env *env, char *var, char *value)
{
	while (env)
	{
		if (ft_strcmp(env->var, var) == 0)
		{
			if (env->value)
				ft_strdel(&env->value);
			if (value != NULL)
				env->value = ft_strdup(value);
			else
				env->value = NULL;
			return (1);
		}
		env = env->next;
	}
	return (0);
}

void	ft_add_var(char *elem, t_env **begin)
{
	t_env	*new_var;
	t_env	*tmp;
	char	**env;

	tmp = *begin;
	env = ft_strsplit(elem, '=');
	new_var = (t_env *)malloc(sizeof(t_env));
	new_var->var = ft_strdup(env[0]);
	new_var->value = NULL;
	if (env[1])
		new_var->value = ft_strdup(env[1]);
	ft_free_array(&env);
	new_var->next = NULL;
	if (*begin == NULL)
		*begin = new_var;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new_var;
	}
}

void	ft_remove_var(t_env *del, t_env **beg)
{
	t_env	*env;
	t_env	*tmp;

	env = *beg;
	while (env && env != del)
	{
		tmp = env;
		env = env->next;
	}
	if (env == *beg)
		*beg = env->next;
	else
		tmp->next = env->next;
	ft_strdel(&env->var);
	ft_strdel(&env->value);
	free(env);
	env = NULL;
}

void	ft_unsetenv(t_env *env, t_command cmd)
{
	int		i;
	t_env	*tmp;

	i = 0;
	if (cmd.ac < 2)
		ft_putendl("unsetenv: Too few arguments");
	else
	{
		while (cmd.av[i])
		{
			tmp = env;
			while (tmp && ft_strcmp(tmp->var, cmd.av[i]))
				tmp = tmp->next;
			if (tmp)
				ft_remove_var(tmp, &env);
			i++;
		}
	}
}

int		ft_setenv(t_env *env, t_command cmd)
{
	int		i;

	i = 0;
	if (cmd.ac == 1)
		ft_print_env(env->next);
	else if (cmd.ac > 3)
		return (ft_setenv_error(1));
	else if (cmd.av[1] && !ft_isalpha((int)*cmd.av[1]))
		return(ft_setenv_error(2));
	else
	{
		while (cmd.av[1][i])
		{
			if (!ft_isalpha((int)cmd.av[1][i]) \
				&& !ft_isdigit((int)cmd.av[1][i]))
				return (ft_setenv_error(3));
			i++;
		}
		ft_set_var(env, cmd.av);
	}
	return (0);
}

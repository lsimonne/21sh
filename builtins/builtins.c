/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 13:36:14 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 04:14:29 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void		ft_builtins(t_sh *sh) // exec instead
{
	t_command *cmd;

	cmd = sh->exec->cmd;
	if (cmd->type == 0)
		ft_exit(sh, cmd);
	else if (cmd->type == 2)
		ft_env(sh->exec);
	else if (cmd->type == 3)
		ft_setenv(sh->exec->env, *cmd);
	else if (cmd->type == 4)
		ft_unsetenv(sh->exec->env, *cmd);
	else if (cmd->type == 1)
		ft_cd(sh->exec);
	else
		ft_echo(cmd);
}

#include "shell.h"

int  ft_exit(t_sh *sh, t_command *cmd)
{
	int i;

	i = 0;
	ft_free_hist(sh->line->hist);
	ft_free_ptree(&sh->parse->ptree);
	free(sh->parse->ptree);
	sh->parse->ptree = NULL;
	free(sh->parse);
	sh->parse = NULL;
	if (cmd->ac > 2)
		return (ft_2many_args("exit"));
	else if (cmd->ac == 2)
	{
		while (cmd->av[1][i])
		{
			if (ft_isdigit(cmd->av[1][i]) == 0 && cmd->av[1][i] != '-')
			{
				ft_putstr_fd("21sh: exit: ", 2);
				ft_putstr_fd(cmd->av[1], 2);
				ft_putendl_fd(": numeric argument required", 2);
				exit(255);
			}
			i++;
		}
		exit(ft_atoi(cmd->av[1])); // maximum value 255 (restricted to 8 bits)
	}
	exit(0);
}

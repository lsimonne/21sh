/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/18 10:41:44 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/27 10:40:17 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_print_env(t_env *env)
{
	t_env	*tmp;

	tmp = env;
	while (tmp)
	{
		ft_putstr(tmp->var);
		ft_putchar('=');
		if (tmp->value)
			ft_putendl(tmp->value);
		else
			ft_putchar('\n');
		tmp = tmp->next;
	}
}

t_env	*ft_list_env(char **envp)
{
	t_env	*env;

	env = NULL;
	while (*envp)
	{
		ft_add_var(*envp, &env);
		envp++;
	}
	return (env);
}

void	ft_set_var(t_env *env, char **var)
{
	char	*elem;
	char	*tmp;

	if (ft_replace_var(env, var[1], var[2]) == 0)
	{
		tmp = ft_strjoin(var[1], "=");
		if (var[2])
		{
			elem = ft_strjoin(tmp, var[2]);
			ft_add_var(elem, &env);
			ft_strdel(&elem);
		}
		else
			ft_add_var(tmp, &env);
		ft_strdel(&tmp);
	}
}

void	ft_sub_env(t_exec *exec, t_sh *sh, int j)
{
	char	**tmp;
	t_command	*cmd;

	cmd = exec->cmd;
	while (cmd->av[j] && ft_strchr(cmd->av[j], '=') != NULL)
	{
		tmp = ft_strsplit(cmd->av[j], '=');
		if (ft_replace_var(sh->exec->env, tmp[0], tmp[1]) == 0)
			ft_add_var(cmd->av[j], &sh->exec->env);
		j++;
	}
	if (cmd->av[j])
	{
		sh->line->line = ft_join_args(cmd->av, j);
		//if (exec->env) ?
		ft_parse(sh);
		ft_execute(sh);
		ft_free_ptree(&sh->parse->root);
		free(sh->parse);
	}
	else
		ft_print_env(sh->exec->env);
}

char	*ft_getenv(char *var, t_env *env)
{
	while (env && ft_strcmp(env->var, var))
		env = env->next;
	if (env)
		return (ft_strdup(env->value));
	return (NULL);
}

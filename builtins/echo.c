#include "shell.h"

static int		ft_operands(char *str)
{
if (str)
	return (1);
	return (0);
}


int		ft_echo(t_command *cmd)
{
	bool	n; // n option not in standards
	int		i;

	ft_putendl("builtin echo");
	i = 1;
	if (ft_strcmp(cmd->av[1], "-n") == 0)
	{
		n = 1;
		i++;
	}
	while (cmd->av[i] && ft_operands(cmd->av[i]) > 0)
	{
		ft_putstr(cmd->av[i]);
		i++;
	}
	ft_putchar('\n');
	return (0);
}

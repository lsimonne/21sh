/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/10 17:19:44 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 15:55:48 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

/* isn't that ft_getenv from somewhere in env? */
static char		*ft_get_value(t_env *env, char *var)
{
	while (env)
	{
		if (ft_strcmp(env->var, var) == 0)
		{
			if (env->value)
				return (ft_strdup(env->value));
		}
		env = env->next;
	}
	return (NULL);
}

static char	*ft_dots(char *path, t_env *env)
{
	char	*pwd;
	char	*tmp;

	if (!(pwd = ft_get_value(env, "PWD")))
	{
		if (!(pwd = getcwd(NULL, 0)))
		{
			ft_cd_error(5, path);
			return (NULL);
		}
	}
	if (*path == '.')
	{
		if (path[1] && path[1] == '.' && (!path[2] || path[2] == '/'))
			return (ft_dot_dot(path, pwd));
		else
		{
			tmp = ft_strdup(path + 1);
			ft_strdel(&path);
			path = ft_strjoin(pwd, tmp);
			ft_strdel(&pwd);
			ft_strdel(&tmp);
		}
	}
	return (path);
}

static int	ft_get_path(t_command cmd, t_env *env, char **path, int dash)
{
	int	i;

	i = ft_after_options(cmd, dash);
	if (!(cmd.av[i]) || (cmd.av[i] && ft_strcmp(cmd.av[i], "~") == 0))
	{
		while (env && ft_strcmp(env->var, "HOME") != 0)
			env = env->next;
		if (env == NULL)
			return (ft_cd_error(1, NULL));
		else
			*path = ft_strdup(env->value);
	}
	else if (ft_strcmp("-", cmd.av[i]) == 0)
	{
		if (!(*path = ft_get_value(env, "OLDPWD")))
			return (ft_cd_error(2, NULL));
	}
	else if (*cmd.av[i] == '.')
		*path = ft_dots(ft_strdup(cmd.av[i]), env);
	else
		*path = ft_strdup(cmd.av[i]);
	if (!*path || dash == -1)
		return (-1);
	return (0);
}

static int	ft_parse_opt(t_cd_opt *opt, t_command cmd)
{
	int	i;
	int	j;

	i = 1;
	while (cmd.av[i] && *cmd.av[i] == '-' && cmd.av[i][1] && (j = 1))
	{
		if (cmd.av[i][j] == '-')
			return (1);
		while (cmd.av[i][j])
		{
			if (cmd.av[i][j] == 'P')
				opt->p = 1;
			else if (cmd.av[i][j] == 'L')
				opt->l = 1;
			else
				return (ft_cd_error(3, &cmd.av[i][j]));
			j++;
		}
		i++;
	}
	if (opt->p == 1)
		opt->l = 0;
	if (cmd.ac - i > 1)
		return (ft_cd_error(4, NULL));
	return (0);
}

int			ft_cd(t_exec *exec)
{
	char		*path;
	char		*cwd;
	t_cd_opt	opt;

	path = NULL;
	opt.l = 0;
	opt.p = 0;
	if (ft_get_path(*exec->cmd, exec->env, &path, ft_parse_opt(&opt, *exec->cmd)) == -1)
		return (-1);
	if (!(cwd = getcwd(NULL, 0)))
		cwd = ft_get_value(exec->env, "PWD");
	if (chdir(path) == -1)
		return (ft_cd_error(5, path));
	ft_replace_pwds(exec->env, cwd, path, opt);
	return (0);
}

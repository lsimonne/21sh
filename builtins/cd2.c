/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd2.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 21:31:41 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 16:02:42 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char	*ft_dot_dot(char *path, char *pwd)
{
	char	*tmp;
	int		len;

	ft_putendl(pwd);
	tmp = ft_strdup(path + 2);
	ft_strdel(&path);
	len = ft_strlen(pwd) - 1;
	if (ft_strchr(pwd, '/') != NULL)
	{
		while (pwd[len] != '/')
			len--;
	}
	path = ft_strsub(pwd, 0, len);
	ft_strdel(&pwd);
	pwd = ft_strjoin(path, tmp);
	ft_strdel(&path);
	ft_strdel(&tmp);
	return (pwd);
}

int		ft_after_options(t_command cmd, int dash)
{
	int	i;

	i = 1;
	while (cmd.av[i] && ft_strlen(cmd.av[i]) > 1 && *cmd.av[i] \
			&& *cmd.av[i] == '-')
	{
		if (dash == 1 && cmd.av[i][1] == '-')
		{
			i++;
			break ;
		}
		i++;
	}
	return (i);
}

void	ft_replace_pwds(t_env *env, char *cwd, char *path, t_cd_opt opt)
{
	char	*tmp;

	if (cwd && (ft_replace_var(env, "OLDPWD", cwd)) == 0)
	{
		tmp = ft_strjoin("OLDPWD=", cwd);
		ft_add_var(tmp, &env);
		ft_strdel(&tmp);
	}
	if (opt.l == 0)
		cwd = getcwd(NULL, 0);
	else
	{
		tmp = ft_strjoin(cwd, "/");
		ft_strdel(&cwd);
		cwd = ft_strjoin(tmp, path);
		ft_strdel(&tmp);
	}
	if ((ft_replace_var(env, "PWD", cwd)) == 0)
	{
		tmp = ft_strjoin("PWD=", cwd);
		ft_add_var(tmp, &env);
		ft_strdel(&tmp);
	}
	ft_strdel(&cwd);
	ft_strdel(&path);
}

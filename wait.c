#include "typedefs.h"
#include "libft/libft.h"

void	ft_save_pid(t_exec *exec, int pid, int nb_pipes)
{
	int	*new;
	int	i;

	i = 0;
	new = (int *)ft_memalloc(sizeof(int) * nb_pipes);
	while (i < nb_pipes - 1)
	{
		new[i] = exec->to_wait[i];
		i++;
	}
	new[i] = pid;
	exec->to_wait = new;
}

void	wait_pids(int *pids, int nb_pipes)
{
	int status;
	int i;

	i = 0;
	while (i < nb_pipes && pids[i])
	{
		waitpid(pids[i], &status, 0);
		i++;
	}
	free(pids);
	pids = NULL;
}
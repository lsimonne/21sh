/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/07 11:25:30 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 04:01:48 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include <dirent.h>

int		ft_check_builtins(t_command *cmd, char **builtins)
{
	int	i;

	i = 0;
	while (builtins[i])
	{
		if (ft_strcmp(cmd->name, builtins[i]) == 0)
			return (i);
		i++;
	}
	return (-1);
}

static int         ft_check_path(t_exec *exec, char **paths)
{
	int             i;
	DIR             *dirp;
	struct dirent   *dirnt;
	char            *tmp;
	char			*path;

	i = 0;
	path = ft_getenv("PATH", exec->env);
	if (!(paths = ft_strsplit(path, ':')))
	{
		ft_strdel(&path);
		return (-1);
	}
	ft_strdel(&path);
	while (paths[i])
	{
		if (!(dirp = opendir(paths[i])))
			return (-1);
		while ((dirnt = readdir(dirp)) != NULL)
		{
			if (exec->cmd->name && ft_strcmp(dirnt->d_name, exec->cmd->name) == 0)
			{
				tmp = ft_strjoin(paths[i], "/");
				exec->cmd->path = ft_strjoin(tmp, exec->cmd->name);
				ft_strdel(&tmp);
				ft_free_array(&paths);
				return (0);
			}
		}
		i++;
	}
	ft_free_array(&paths);
	return (-1);
}

int			ft_parse_redir(t_tkns *tokens, t_exec *exec)
{
	int		nb_args;

	// [unquoted fd] reidr-op word
	ft_init_redir(exec);
	nb_args = 0;
	while (tokens)
	{
		if (tokens->type == TK_REDIR || (tokens->next && tokens->next->type == TK_REDIR && ft_isnum(tokens->data))) // if number unquoted
		{
			/* error if multiple redir in same direction ?
			if (exec->redir[0] || exec->redir[1])
			{
				ft_putendl("Ambiguous output redirect"); // error func
				return (-1);
			}*/
			ft_set_redir(exec, &tokens);
		}
		else
		{
			tokens->type = 4;
			nb_args++;
		}
		if (tokens)
			tokens = tokens->next;
	}
	//debug_redir(exec->cmd->redir);
	return (nb_args);
}

static int		ft_init_command(int nb_tkns, t_command *cmd)
{
	cmd->path = NULL;
	cmd->ac = nb_tkns; // modify this in redir
	if (!(cmd->av = (char **)ft_memalloc(sizeof(char *) * (cmd->ac + 1))))
		return (-1); // malloc error
	cmd->av[cmd->ac] = NULL;
	return (0);
}

int			ft_parse_command(t_tkns **tokens, t_command *cmd, int n)
{
	int		i;
	t_tkns	*tmp;

	i = 0;
	ft_init_command((*tokens)->nb_tkns, cmd);
	cmd->ac = n;
	while (i < cmd->ac && *tokens)
	{
		if ((*tokens)->type == 4)
		{
			cmd->av[i] = ft_strdup((*tokens)->data);
			i++;
		}
		tmp = *tokens;
		*tokens = (*tokens)->next;
		ft_strdel(&tmp->data);
		free(tmp);
		tmp = NULL;
	}
	cmd->name = ft_strdup(cmd->av[0]);
	return (0);
}

int         ft_get_cmd(t_sh *sh, t_command *cmd)
{
	int     code;
	int		n;
	char    *paths;

	if ((n = ft_parse_redir(sh->parse->ptree->token, sh->exec)) < 0)
		return (-1);
	ft_parse_command(&sh->parse->ptree->token, cmd, n);
	if (*cmd->av)
	{
		if ((code = ft_check_builtins(cmd, sh->builtins)) >= 0)
			return (code);
		if (ft_check_path(sh->exec, &paths) == 0)
			return (NB_BUILTINS);
		if (access(cmd->av[0], X_OK) == 0)
		{
			cmd->path = ft_strdup(cmd->av[0]);
			return (NB_BUILTINS + 1);
		}
		if (access(cmd->av[0], F_OK) == 0)
			return (-2);
	}
	free(sh->exec->redir[0]);
	free(sh->exec->redir[1]);
	return (-1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 15:07:15 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/09 16:53:33 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HISTORY_H
#define HISTORY_H

#include "shell.h"
#include <fcntl.h>

#define HISTFILE ".21sh_history"
#define HISTSIZE 100

int		ft_add_to_hist(t_line *line);
int		ft_up(t_line *line);
int		ft_down(t_line *line);
int		ft_init_hist(t_line *line);

#endif

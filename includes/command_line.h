/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_line.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 17:09:42 by lsimonne          #+#    #+#             */
/*   Updated: 2016/09/09 16:54:06 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMAND_LINE_H
# define COMMAND_LINE_H

#include "shell.h"
#include <term.h>
#include <curses.h>

/******   typedef enum instead of ugly defines*/

#define LEFT_KEY 4479771
#define RIGHT_KEY 4414235
#define UP_KEY 4283163
#define DOWN_KEY 4348699
#define RET_KEY 10
#define DEL_KEY 127
#define TAB_KEY 9
#define CTRL_D 4
#define CTRL_L 12
#define HOME_KEY 4741915
#define END_KEY 4610843
#define OPT_LEFT 74995417045787//993090331174659
#define OPT_RIGHT 993090331172099

/*
**					termcaps.c
*/

int					ft_raw_term(void);
int     			ft_restore_term();
int					ft_get_caps(t_term *termcaps);
int					ft_outc(int c);
void				ft_init_term(t_sh *sh);


/*
**					move.c
*/

int					ft_left(t_line *line);
int					ft_right(t_line *line);
int					ft_home(t_line *line);
int					ft_end(t_line *line);

/*
**					w_move.c
*/

int					ft_word_left(t_line *line);

#endif

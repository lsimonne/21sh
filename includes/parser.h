/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 10:53:12 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 15:13:49 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

/*
**				errors.c
*/

int				ft_token_error(char c);

/*
**				tools.c
*/

int				ft_is_metachar(char c);
int				ft_is_blank(char c);

/*
**				parse_tree.c
*/

int				ft_add_to_tree(t_parse *parse, t_tkns *new_token);


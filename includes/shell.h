/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 13:12:46 by lsimonne          #+#    #+#             */
/*   Updated: 2016/11/22 04:11:36 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H

# define SHELL_H

#include "typedefs.h"
# include "../libft/libft.h"
#include "../libft/get_next_line.h"
#include "command_line.h" // no
#include "history.h"
# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <sys/wait.h>
# include <stdbool.h>

#define NB_BUILTINS 6
#define RED "\033[31m"
#define DEF_COL "\033[0m"

#define STDOUT 1
#define STDERR 2
//#define BUFF_SIZE 512
#define HISTSIZE 100

/*
**					debug.c
*/

void				debug_tokens(t_tkns *tokens);
void				debug_redir(t_redir **redir);
void				debug_init(t_sh *sh);
void				debug_hist(t_line *line);
void				debug_command(t_command *cmd);
void				debug_line(t_line *line);
void				display_quote_enum();

/*
**					free.c
*/

void				ft_free_cmd(t_command **cmd);
void				ft_free_env(t_env **env);
void				ft_free_exec(t_exec **exec);
void				ft_free_hist(char **hist);
void				ft_free_ptree(t_parse_tree **root);
void				ft_free_sh(t_sh **sh);

/*
** 					signals.c
*/

void				ft_signals(int signal);

/*
** 					errors.c
*/

int					ft_wrong_command(int code, t_command **cmd);
int					ft_arg_error(int code, char *path);
int					ft_2many_args(char *command);



/*					 21sh.c
**
*/

int					ft_prompt(t_sh *sh);
int					ft_command(t_sh *sh);

/*
**					prompt.c
*/

int					ft_get_prompt(char **prompt);

/*
**					LINE EDITION
*/

/*
**					line_edit.c
*/

t_keys				*ft_keys_list(void);
int					ft_key_handler(t_line *line, t_keys *key_funcs);

/*
**					parse.c
*/

int					ft_build_tree(t_parse *parse);
int					ft_parse(t_sh *sh);

/*
**					tokenizer.c
*/

int					ft_tokenizer(t_sh *sh, char *line);

/*
**					EXECUTION
*/

/*
**  				command.c
*/

int					ft_get_cmd(t_sh *sh, t_command *cmd);

/*
** 					execute.c
*/

int					ft_execute(t_sh *sh);

/*
**					pipe.c
*/

int					ft_run_pipe(t_sh *sh);

/*
**					redir.c
*/

int					ft_handle_redir(t_exec *exec);
int					ft_set_redir(t_exec *exec, t_tkns **tokens);
int					ft_init_redir(t_exec * exec);

/*
**					close_free_redirs.c
*/

void			close_redirs(t_exec *exec);
void			free_redirs(t_exec *exec);


/*
**					heredoc.c
*/

int					ft_start_hdoc(char *delimiter, int fd);

/*
**					wait.c
*/

void				ft_save_pid(t_exec *exec, int pid, int nb_pipes);
void				wait_pids(int *pids, int nb_pipes);

/*
**					init.c
*/

int					ft_init(t_sh *sh, char **envp);
int					ft_init_basics(t_sh *sh);
int					ft_init_line(t_line *line, size_t p_len);



/************************************ BUILTINS *****************************************/

/*
** 					builtins.c
*/

void				ft_builtins(t_sh *sh);
int					ft_exit(t_sh *sh, t_command *cmd);

/*
**					cd.c
*/

int				ft_cd(t_exec *exec);
//char				*ft_get_value(t_env *env, char *var);


/*
**					cd2.c
*/

char				*ft_dot_dot(char *path, char *pwd);
int					ft_after_options(t_command cmd, int dash);
void				ft_replace_pwds(t_env *env, char *cwd, char *path, \
					t_cd_opt opt);

/*
** 					env.c
*/

void				ft_add_var(char *elem, t_env **begin);
char				**ft_env_array(t_env *env);
int					ft_env(t_exec *exec);
char				*ft_join_args(char **args, int i);

/*
** 					env2.c
*/

int					ft_setenv(t_env *env, t_command cmd);
int					ft_replace_var(t_env *env, char *var, char *value);
void				ft_unsetenv(t_env *env, t_command cmd);

/*
** 					env3.c
*/

void				ft_print_env(t_env *env);
void				ft_set_var(t_env *env, char **var);
t_env				*ft_list_env(char **envp);
void				ft_sub_env(t_exec *exec, t_sh *sh, int j);
char				*ft_getenv(char *var, t_env *env);


/*
** 					echo
*/

int					ft_echo(t_command *cmd);

/*
**					builtins/errors.c
*/

int					ft_cd_error(int code, char *path);
int					ft_env_opt_err(char opt);
int					ft_setenv_error(int code);

#endif

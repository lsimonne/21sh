/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typedefs.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 16:45:39 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/07 16:18:32 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef TYPEDEFS_H
# define TYPEDEFS_H

#include <stdbool.h>
#include <stdlib.h>

#define HISTSIZE 100



/*
** TERMCAPS
**
	le = move cursor left one col
	nd = move curs right one col
	ce = clear to end of line
	sc = save cursor position
	rc = return to saved cur pos
	im = insert mode
	ei = leave insert mode
	ic = insert one char without moving cur
	dc = delete one char
**
**
**
*/

/*
**				line_edition
*/

typedef struct 		s_term {

	char    		*le;
	char    		*nd;
	char    		*ce;
	char    		*sc;
	char    		*rc;
	char    		*im;
	char    		*ei;
	char    		*ic;
	char    		*dc;
	//char  *ve; //cursor visible
	//char  *cd; // clear line curs is on + all below
	//char  *cm; // move to pos x,y
}           		t_term;

typedef struct		s_line t_line;

typedef struct      s_keys {

	long             key;
	int             (*f)(t_line *line);
	struct s_keys   *next;

}                   t_keys;

typedef struct s_line {

	t_term          *termcaps;
	t_keys      	*key_funcs;
	char      		*hist[HISTSIZE];
	char            *line;
	int				hist_i;
	int				hist_n;
	char            *saved;
	long            key;
	int				prev_key;
	size_t          cursor;
	size_t          p_len;
	size_t          len;
	//struct winsize    ws;

}               t_line;

/*
**					execution
*/

typedef struct		s_hdoc
{
	char			*line;
	struct s_hdoc	*next;
}					t_hdoc;

typedef struct		s_redir
{
	int		type;
	int		to_redir;
	int		fd_out;
}					t_redir;

typedef struct      s_env
{
	char            *var;
	char            *value;
	struct s_env    *next;
}                   t_env;

typedef struct      s_command
{
	int				type;
	char            *path;
	char            *name;
	int             ac;
	char            **av;
}                   t_command;

typedef struct		s_exec {

	t_command		*cmd;
	t_redir			*redir[2];
	t_env			*env;
	int 			*to_wait;
	int				saved_stdin;
	int				saved_stdout;
	int				curr_stdin;
	int				curr_stdout;
	int				fildes[2];
	int				nb_pipes;
	// parse_tree
}					t_exec;

/*
**					parse
*/

typedef enum		tokens{

	TK_WORD,
	TK_CTRLOP,
	TK_REDIR,
	TK_AGGR,
	TK_ARG,
}					e_tokens;

typedef enum		quotes{

	NO_QU,
	QU_SIMPLE,
	QU_DOUBLE,
}					e_quotes;

typedef struct		s_tkns {

	char			*data;
	e_tokens		type;
	size_t			nb_tkns;
	struct s_tkns	*next;
}					t_tkns;

typedef struct		s_parse_tree {

	t_tkns				*token;
	struct s_parse_tree *parent;
	struct s_parse_tree *left;
	struct s_parse_tree *right;
}					t_parse_tree;

typedef struct		s_parse{

	e_quotes		curr_quote;
	t_parse_tree	*ptree;
	t_parse_tree	*root;
}					t_parse;

/*
**					SH
*/

typedef struct      s_sh{

	char        	*prompt;
	char			*ops;
	t_line      	*line;
	t_parse			*parse;
	t_exec			*exec;
	char        	**builtins; // make func pointers
	//char			**paths; // init from check_path in command.c + hashtable for execution
}                   t_sh;

/*
**					builtins
*/

typedef struct      s_cd_opt
{
	bool    p;
	bool    l;
}                  t_cd_opt;

typedef struct      s_env_opt
{
	bool    i;
}                   t_env_opt;

#endif
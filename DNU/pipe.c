/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 10:46:18 by lsimonne          #+#    #+#             */
/*   Updated: 2016/10/04 16:35:56 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include "shell.h"
#include "unistd.h"
#include "stdlib.h"

void		ft_run_pipe(char **envp)//(t_sh *sh)
{
	pid_t	child;
	char	*cmd;
	char	*cmd2;
	char	*args[3];
	char	*args2[3];
	int	fildes[2];



	child = -1;
	cmd = "/bin/ls";
	cmd2 = "/bin/cat";
	args[0] = cmd;
	args2[0] = cmd2;
	args[1] = "-l";
	args2[1] = "-e";
	args[2] = NULL;
	args2[2] = NULL;
	pipe(fildes);
	child = fork(); // or builtin
	if (child == 0)
	{
		dup2(fildes[1], STDOUT_FILENO); // write_end, STDOUT
		close(fildes[0]);
		execve(cmd, args, envp);
	}
	if (child > 0)
	{
		dup2(fildes[0], STDIN_FILENO); // read_end, STDIN
		close(fildes[1]);
		wait(0);
		execve(cmd2, args2, envp);
		return;
	}
}

/*
int		ft_exec(t_sh *sh)
{*/
int main(int ac, char **av, char **envp)
{
	pid_t child;

	child = -1;
	child = fork(); // or builtin

	// child
	if (child == 0)
	{
		/*if (sh->ptree->token->type == 1)
		{
			if (!ft_strcmp(sh->ptree->token->data, "|")) // no
			{*/
				ft_run_pipe(envp);
			//}
		}
	// parent
	if (child > 0)
		wait(0);
	return (0);
}
